<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-url-redirecter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UrlRedirecter;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;

/**
 * RedirecterFactory class file.
 * 
 * This class is a simple implementation of the RedirecterFactoryInterface.
 * 
 * @author Anastaszor
 */
class RedirecterFactory implements RedirecterFactoryInterface
{
	
	/**
	 * The client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The request factory.
	 * 
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The uri factory.
	 * 
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * Builds a new RedirecterFactory with the given client, request factory
	 * and uri factory.
	 * 
	 * @param ClientInterface $client
	 * @param RequestFactoryInterface $requestFactory
	 * @param UriFactoryInterface $uriFactory
	 */
	public function __construct(ClientInterface $client, RequestFactoryInterface $requestFactory, UriFactoryInterface $uriFactory)
	{
		$this->_client = $client;
		$this->_requestFactory = $requestFactory;
		$this->_uriFactory = $uriFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UrlRedirecter\RedirecterFactoryInterface::createRedirecter()
	 */
	public function createRedirecter(?RedirecterFactoryConfigurationInterface $configuration) : RedirecterInterface
	{
		if(null === $configuration)
		{
			$configuration = new RedirecterFactoryConfiguration();
		}
		
		$redirecter = new RedirecterChain();
		
		$redirecter->addRedirecter(new GenericRedirecter($this->_client, $this->_requestFactory, $this->_uriFactory));
		
		if(!$configuration->isWhitelisted('atu.ca'))
		{
			$redirecter->addRedirecter(new AtuCaRedirecter($this->_client, $this->_requestFactory, $this->_uriFactory));
		}
		
		if(!$configuration->isWhitelisted('qr.net'))
		{
			$redirecter->addRedirecter(new QrNetRedirecter($this->_client, $this->_requestFactory, $this->_uriFactory));
		}
		
		if(!$configuration->isWhitelisted('security-links.com'))
		{
			$redirecter->addRedirecter(new SecuritylinksComRedirecter($this->_client, $this->_requestFactory, $this->_uriFactory));
		}
		
		if(!$configuration->isWhitelisted('ushortlinks.com'))
		{
			$redirecter->addRedirecter(new UshortlinksComRedirecter($this->_client, $this->_requestFactory, $this->_uriFactory));
		}
		
		return $redirecter;
	}
	
}
