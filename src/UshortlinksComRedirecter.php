<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-url-redirecter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UrlRedirecter;

use ArrayIterator;
use InvalidArgumentException;
use Iterator;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;

/**
 * UshortlinksComRedirecter class file.
 *
 * This class retrieve links from the ushort-links.com links.
 *
 * @author Anastaszor
 */
class UshortlinksComRedirecter implements RedirecterInterface
{
	
	/**
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * Builds a new AtuCaRedirecter with the given client and factories.
	 *
	 * @param ClientInterface $client
	 * @param RequestFactoryInterface $requestFactory
	 * @param UriFactoryInterface $uriFactory
	 */
	public function __construct(ClientInterface $client, RequestFactoryInterface $requestFactory, UriFactoryInterface $uriFactory)
	{
		$this->_client = $client;
		$this->_requestFactory = $requestFactory;
		$this->_uriFactory = $uriFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UrlRedirecter\RedirecterInterface::accept()
	 */
	public function accept(?UriInterface $sourceUrl) : bool
	{
		return null !== $sourceUrl
			&& (bool) \preg_match('#www\\.ushort-links\\.com$#', $sourceUrl->getHost());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UrlRedirecter\RedirecterInterface::getRedirections()
	 */
	public function getRedirections(?UriInterface $sourceUrl) : Iterator
	{
		if(null === $sourceUrl || !$this->accept($sourceUrl))
		{
			return new ArrayIterator();
		}
		
		try
		{
			$sourceUrl->withScheme('http');
		}
		catch(InvalidArgumentException $exc)
		{
			// nothing to do
		}
		
		$request = $this->_requestFactory->createRequest('GET', $sourceUrl);
		
		try
		{
			// disable follow location option on the client
			$request = $request->withAddedHeader('X-Php-Follow-Location', '0');
		}
		catch(InvalidArgumentException $exc)
		{
			// nothing to do
		}
		
		try
		{
			$response = $this->_client->sendRequest($request);
		}
		catch(ClientExceptionInterface $exc)
		{
			return new ArrayIterator();
		}
		
		$uris = $this->getUrisFromContents($response);
		
		return new ArrayIterator($uris);
	}
	
	/**
	 * Gets the uris from the response contents.
	 * 
	 * @param ResponseInterface $response
	 * @return array<integer, UriInterface>
	 */
	public function getUrisFromContents(ResponseInterface $response) : array
	{
		$uris = [];
		$res = $response->getBody()->__toString();
		$res = \str_replace("\n", '', $res);
		
		// find the tag  <meta http-equiv="refresh" content="4; URL=_the_url_here_" />
		$pos = \mb_strpos($res, 'display:none;">');
		if(false !== $pos)
		{
			$rpos = \mb_strpos($res, '</div>', $pos + 4);
			if(false !== $rpos)
			{
				$links = \trim((string) \mb_substr($res, $pos + 15, $rpos - $pos - 15));
				
				$matches = [];
				if(0 < (int) \preg_match_all('#<a.+?href="(.+?)".+?>.+?</a>#', $links, $matches, \PREG_PATTERN_ORDER))
				{
					/** @phpstan-ignore-next-line */
					if(isset($matches[1]))
					{
						$urls = \array_unique(\array_map('trim', $matches[1]));
						
						foreach($urls as $url)
						{
							try
							{
								$uris[] = $this->_uriFactory->createUri($url);
							}
							catch(InvalidArgumentException $exc)
							{
								// ignore
							}
						}
					}
				}
			}
		}
		
		return $uris;
	}
	
}
