<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-url-redirecter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UrlRedirecter;

/**
 * RedirecterFactoryConfiguration class file.
 * 
 * This class is a simple implementation of the RedirecterFactoryConfigurationInterface.
 * 
 * @author Anastaszor
 */
class RedirecterFactoryConfiguration implements RedirecterFactoryConfigurationInterface
{
	
	/**
	 * The whitelist of domains.
	 * 
	 * @var array<string, integer>
	 */
	protected array $_whitelist = [];

	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UrlRedirecter\RedirecterFactoryConfigurationInterface::addToWhitelist()
	 */
	public function addToWhitelist(string $domain) : static
	{
		$this->_whitelist[$domain] = 1;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UrlRedirecter\RedirecterFactoryConfigurationInterface::isWhitelisted()
	 */
	public function isWhitelisted(string $domain) : bool
	{
		return isset($this->_whitelist[$domain]);
	}
	
}
