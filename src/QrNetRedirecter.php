<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-url-redirecter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UrlRedirecter;

use ArrayIterator;
use InvalidArgumentException;
use Iterator;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;

/**
 * QrNetRedirecter class file.
 *
 * This class retrieve links from the qr.net links.
 *
 * @author Anastaszor
 */
class QrNetRedirecter implements RedirecterInterface
{
	
	/**
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * Builds a new AtuCaRedirecter with the given client and factories.
	 *
	 * @param ClientInterface $client
	 * @param RequestFactoryInterface $requestFactory
	 * @param UriFactoryInterface $uriFactory
	 */
	public function __construct(ClientInterface $client, RequestFactoryInterface $requestFactory, UriFactoryInterface $uriFactory)
	{
		$this->_client = $client;
		$this->_requestFactory = $requestFactory;
		$this->_uriFactory = $uriFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UrlRedirecter\RedirecterInterface::accept()
	 */
	public function accept(?UriInterface $sourceUrl) : bool
	{
		return null !== $sourceUrl
			&& (bool) \preg_match('#^qr\\.net/.+$#', $sourceUrl->getHost());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UrlRedirecter\RedirecterInterface::getRedirections()
	 */
	public function getRedirections(?UriInterface $sourceUrl) : Iterator
	{
		if(null === $sourceUrl || !$this->accept($sourceUrl))
		{
			return new ArrayIterator();
		}
		
		// normalization
		$path = \str_replace('/code/', '/', (string) \preg_replace('#\\.(png|svg)$#', '', $sourceUrl->getPath()));
		
		try
		{
			$sourceUrl = $sourceUrl->withPath($path);
		}
		catch(InvalidArgumentException $exc)
		{
			// nothing to do
		}
		
		try
		{
			$sourceUrl = $sourceUrl->withScheme('http');
		}
		catch(InvalidArgumentException $exc)
		{
			// nothing to do
		}
		
		$request = $this->_requestFactory->createRequest('GET', $sourceUrl);
		
		try
		{
			// disable follow location option on the client
			$request = $request->withAddedHeader('X-Php-Follow-Location', '0');
		}
		catch(InvalidArgumentException $exc)
		{
			// nothing to do
		}
		
		try
		{
			$response = $this->_client->sendRequest($request);
		}
		catch(ClientExceptionInterface $exc)
		{
			return new ArrayIterator();
		}
		
		$uris = [];
		$locationHeader = $response->getHeader('Location');
		if(0 < \count($locationHeader))
		{
			foreach($locationHeader as $locationPart)
			{
				try
				{
					$uris[] = $this->_uriFactory->createUri($locationPart);
				}
				catch(InvalidArgumentException $exc)
				{
					// ignore
				}
			}
		}
		
		return new ArrayIterator($uris);
	}
	
}
