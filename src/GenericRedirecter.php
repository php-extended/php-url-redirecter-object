<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-url-redirecter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UrlRedirecter;

use ArrayIterator;
use InvalidArgumentException;
use Iterator;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;

/**
 * GenericRedirecter class file.
 *
 * This class retrieve links from 30X redirecter links.
 *
 * @author Anastaszor
 */
class GenericRedirecter implements RedirecterInterface
{
	
	/**
	 * All redirecter domains that we already know the redirection domain.
	 * Domains in this list should also be registered into the _acceptedHttp*
	 * lists to be taken into account.
	 *
	 * @var array<string, array{"match": string, "redirect": string}>
	 */
	protected array $_autoRedirects = [
		'clkme.in' => [
			'match' => 'clkme\\.in/(.+)',
			'redirect' => 'http://cllkme.com/$1',
		],
		'cur.lv' => [
			'match' => 'cur\\.lv/([^.]+)$',
			'redirect' => 'http://cur.lv/redirect_curlv.php?code=$1&ticket=&r=',
		],
		'dereferer.xyz' => [
			'match' => 'dereferer\\.xyz/\\?(.+)$',
			'redirect' => '$1',
		],
		'lmgtfy.com' => [
			'match' => 'lmgtfy\\.com/\\?q=(.+)$',
			'redirect' => 'https://google.com/?q=$1',
		],
		'youtu.be' => [
			'match' => 'youtu\\.be/([\\w\\d_-]+)',
			'redirect' => 'https://youtube.com/watch?v=$1',
		],
	];
	
	/**
	 * All redirecter domains that accepts https.
	 *
	 * @var array<string, string>
	 */
	protected array $_acceptedHttps = [
		'app.link' => 'app\\.link',
		'bit.do' => 'bit\\.do',
		'bit.ly' => 'bit\\.ly',
		'buff.ly' => 'buff\\.ly',
		'clk.im' => 'clk\\.im',
		'geni.us' => 'geni\\.us',
		'goo.gl' => 'goo\\.gl',
		'is.gd' => 'is\\.gd',
		'ity.im' => 'ity\\.im',
		'lmgtfy.com' => 'lmgtfy\\.com',
		'ovh.to' => 'ovh\\.to',
		'po.st' => 'po\\.st',
		'psbe.co' => 'psbe\\.co',
		'rebrand.ly' => 'rebrand\\.ly',
		'short.cm' => 'short\\.cm',
		'smarturl.it' => 'smarturl\\.it',
		'snip.ly' => 'snip\\.ly',
		'snipli.com' => 'snipli\\.com',
		't.co' => 't\\.co',
		'tiny.cc' => 'tiny\\.cc',
		'tinyurl.com' => 'tinyurl\\.com',
		'tr.im' => 'tr\\.im',
		'urlsgalore.com' => 'urlsgalore\\.com',
		'v.gd' => 'v\\.gd',
		'youtu.be' => 'youtu\\.be',
	];
	
	/**
	 * All redirecter domains that does not supports https.
	 *
	 * @var array<string, string>
	 */
	protected array $_acceptedHttp = [
		'9nl.at' => '9nl\\.at',
		'beam.to' => 'beam\\.to',
		'bfy.tw' => 'bfy\\.tw',
		'clkme.in' => 'clkme\\.in',
		'cutt.us' => 'cutt\\.us',
		'cur.lv' => 'cur\\.lv',
		'easyurl.net' => 'easyurl\\.net',
		'georiot.co' => 'georiot\\.co',
		'goshrink.com' => 'goshrink\\.com',
		'hyperurl.co' => 'hyperurl\\.co',
		'mcaf.ee' => 'mcaf\\.ee',
		'ow.ly' => 'ow\\.ly',
		'rebrandly.click' => 'rebrandly\\.click',
		'rebrandly.xyz' => 'rebrandly\\.xyz',
		'redirects.ca' => 'redirects\\.ca',
		'shorl.com' => 'shorl\\.com',
		'slink.co' => 'slink\\.co',
		'snip.li' => 'snip\\.li',
		'tiny.pl' => 'tiny\\.pl',
		'u.to' => 'u\\.to',
		'utm.io' => 'utm.io',
	];
	
	/**
	 * All redirecter domains for which we have to extract the url from the
	 * html document.
	 * Domains in this list should also be registered into the _accepted_http*
	 * lists to be taken into account.
	 *
	 * @var array<string, array{"start": string, "end": string}>
	 */
	protected array $_bodySearch = [
		'beam.to' => [
			// find the tag : <iframe src="___the_url_here___" />
			'start' => '<iframe src="',
			'end' => '"',
		],
		'clk.im' => [
			// find the tag : <meta http-equiv="refresh" content="0;URL='___the_url_here___'" />
			'start' => '<meta http-equiv="refresh" content="0;URL=\'',
			'end' => "'",
		],
		'cur.lv/redirect_curlv.php' => [
			// find the tag : <iframe frameborder="0" marginheight="0" marginwidth="0" width="100%" height="65" sandbox="allow-same-origin allow-scripts allow-top-navigation" src="___the_url_here___" scrolling="no"></iframe>
			'start' => 'allow-top-navigation" src="',
			'end' => '"',
		],
		'cur.lv/ntop.php' => [
			// find the tag : <span style="font-weight: bolder;">___the_url_here___</span>
			'start' => '<span style="font-weight: bolder;">',
			'end' => '</span>',
		],
		'easyurl.net' => [
			// find the tag : <frame name="main" src="___the_url_here___">
			'start' => '<frame name="main" src="',
			'end' => '"',
		],
		'goshrink.com' => [
			// find the tag : <frame name="main" src="___the_url_here___">
			'start' => '<frame name="main" src="',
			'end' => '"',
		],
		'ity.im' => [
			// find the script : window.open("___the_url_here___")
			'start' => 'window.open("',
			'end' => '")',
		],
		'mcaf.ee' => [
			// find the tag : <frame src="___the_url_here___" name="mainFrame" />
			'start' => '<frame src="',
			'end' => '"',
		],
		'redirects.ca' => [
			// find the tag : <frame name="main" src="___the_url_here___">
			'start' => '<frame name="main" src="',
			'end' => '"',
		],
		'snip.ly' => [
			// find the tag : <meta property="og:url" content="___the_url_here___" />
			'start' => '<meta property="og:url" content="',
			'end' => '"',
		],
		'shorl.com' => [
			// find the tag : <meta http-equiv="refresh" content="4; URL=___the_url_here___" />
			'start' => '"4; URL=',
			'end' => '"',
		],
		'v.gd' => [
			// find the tag : <div class="biglink">___the_url_here___</div>
			'start' => 'class="biglink">',
			'end' => '<',
		],
	];
	
	/**
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * Builds a new GenericRedirecter with the given client and factories.
	 *
	 * @param ClientInterface $client
	 * @param RequestFactoryInterface $requestFactory
	 * @param UriFactoryInterface $uriFactory
	 */
	public function __construct(ClientInterface $client, RequestFactoryInterface $requestFactory, UriFactoryInterface $uriFactory)
	{
		$this->_client = $client;
		$this->_requestFactory = $requestFactory;
		$this->_uriFactory = $uriFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UrlRedirecter\RedirecterInterface::accept()
	 */
	public function accept(?UriInterface $sourceUrl) : bool
	{
		if(null === $sourceUrl)
		{
			return false;
		}
		
		foreach(\array_keys($this->_autoRedirects) as $domain)
		{
			if($sourceUrl->getHost() === $domain)
			{
				return true;
			}
		}
		
		foreach($this->_acceptedHttp as $domain)
		{
			if($sourceUrl->getHost() === $domain)
			{
				return true;
			}
		}
		
		foreach($this->_acceptedHttps as $domain)
		{
			if($sourceUrl->getHost() === $domain)
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UrlRedirecter\RedirecterInterface::getRedirections()
	 */
	public function getRedirections(?UriInterface $sourceUrl) : Iterator
	{
		if(null === $sourceUrl || !$this->accept($sourceUrl))
		{
			return new ArrayIterator();
		}
		
		$uris = $this->getUrisFromAutoRedirects($sourceUrl);
		if(!empty($uris))
		{
			return new ArrayIterator($uris);
		}
		
		$http = $this->shouldForceHttp($sourceUrl);
		
		try
		{
			$sourceUrl = $sourceUrl->withScheme('http'.($http ? '' : 's'));
		}
		catch(InvalidArgumentException $exc)
		{
			// nothing to do
		}
		
		$request = $this->_requestFactory->createRequest('GET', $sourceUrl);
		
		try
		{
			// disable follow location option on the client
			$request = $request->withAddedHeader('X-Php-Follow-Location', '0');
		}
		catch(InvalidArgumentException $exc)
		{
			// nothing to do
		}
		
		try
		{
			$response = $this->_client->sendRequest($request);
		}
		catch(ClientExceptionInterface $exc)
		{
			return new ArrayIterator();
		}
		
		$uris = $this->getUrisFromLocationHeader($response);
		if(!empty($uris))
		{
			return new ArrayIterator($uris);
		}
		
		$uris = $this->getUrisFromContents($sourceUrl, $response);
		
		return new ArrayIterator($uris);
	}
	
	/**
	 * Gets the uris from the auto redirects.
	 * 
	 * @param UriInterface $sourceUrl
	 * @return array<integer, UriInterface>
	 */
	public function getUrisFromAutoRedirects(UriInterface $sourceUrl) : array
	{
		$uris = [];
		
		foreach($this->_autoRedirects as $domain => $params)
		{
			if($sourceUrl->getHost() === $domain)
			{
				$matches = [];
				if(\preg_match('#^https?://'.$params['match'].'$#', $sourceUrl->__toString(), $matches))
				{
					if(isset($matches[1]))
					{
						$match = \str_replace('$1', $matches[1], $params['redirect']);
						
						try
						{
							$uris[] = $this->_uriFactory->createUri($match);
						}
						catch(InvalidArgumentException $exc)
						{
							// nothing to do
						}
					}
				}
			}
		}
		
		return $uris;
	}
	
	/**
	 * Checks whether the source url should be send in http not secure.
	 * 
	 * @param UriInterface $sourceUrl
	 * @return boolean
	 */
	public function shouldForceHttp(UriInterface $sourceUrl) : bool
	{
		foreach(\array_keys($this->_acceptedHttp) as $domain)
		{
			$pos = \mb_strpos($sourceUrl->getHost(), $domain);
			// sanity check, the position is in the first part of the string
			// i.e. we check the domain, not some late part of the url
			if(false !== $pos && 2 * (int) \mb_strlen($domain) > $pos)
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Gets the uris from the location header.
	 * 
	 * @param ResponseInterface $response
	 * @return array<integer, UriInterface>
	 */
	public function getUrisFromLocationHeader(ResponseInterface $response) : array
	{
		$uris = [];
		$locationHeader = $response->getHeader('Location');
		if(0 < \count($locationHeader))
		{
			foreach($locationHeader as $locationPart)
			{
				try
				{
					$uris[] = $this->_uriFactory->createUri($locationPart);
				}
				catch(InvalidArgumentException $exc)
				{
					// ignore
				}
			}
		}
		
		return $uris;
	}
	
	/**
	 * Gets the uris from the contents.
	 * 
	 * @param UriInterface $sourceUrl
	 * @param ResponseInterface $response
	 * @return array<integer, UriInterface>
	 */
	public function getUrisFromContents(UriInterface $sourceUrl, ResponseInterface $response) : array
	{
		$uris = [];
		$res = $response->getBody()->__toString();
		
		foreach($this->_bodySearch as $domain => $searchParams)
		{
			if(false !== \mb_strpos($sourceUrl->getHost(), $domain))
			{
				$start = $searchParams['start'];
				$end = $searchParams['end'];
				$startlen = (int) \mb_strlen($start);
				$pos = \mb_strpos($res, $start);
				if(false === $pos)
				{
					continue;
				}
				
				$rpos = \mb_strpos($res, $end, $pos + 1 + $startlen);
				if(false === $rpos)
				{
					continue;
				}
				
				$uri = \trim((string) \mb_substr($res, $pos + $startlen, $rpos - $pos - $startlen));
				
				try
				{
					$uris[] = $this->_uriFactory->createUri($uri);
				}
				catch(InvalidArgumentException $exc)
				{
					// ignore
				}
			}
		}
		
		return $uris;
	}
	
}
