<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-url-redirecter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UrlRedirecter;

use ArrayIterator;
use InvalidArgumentException;
use Iterator;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;

/**
 * AtuCaRedirecter class file.
 *
 * This class retrieve links from the atu.ca links.
 *
 * @author Anastaszor
 */
class AtuCaRedirecter implements RedirecterInterface
{
	
	/**
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * Builds a new AtuCaRedirecter with the given client and factories.
	 * 
	 * @param ClientInterface $client
	 * @param RequestFactoryInterface $requestFactory
	 * @param UriFactoryInterface $uriFactory
	 */
	public function __construct(ClientInterface $client, RequestFactoryInterface $requestFactory, UriFactoryInterface $uriFactory)
	{
		$this->_client = $client;
		$this->_requestFactory = $requestFactory;
		$this->_uriFactory = $uriFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UrlRedirecter\RedirecterInterface::accept()
	 */
	public function accept(?UriInterface $sourceUrl) : bool
	{
		return null !== $sourceUrl
			&& (bool) \preg_match('#^atu\\.ca/.+$|^.+\\.atu\\.ca/?$#', $sourceUrl->getHost());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UrlRedirecter\RedirecterInterface::getRedirections()
	 */
	public function getRedirections(?UriInterface $sourceUrl) : Iterator
	{
		if(null === $sourceUrl || !$this->accept($sourceUrl))
		{
			return new ArrayIterator();
		}
		
		$sourceUrl = $this->normalizeUrl($sourceUrl);
		
		$request = $this->_requestFactory->createRequest('GET', $sourceUrl);
		
		try
		{
			// disable follow location option on the final client
			$request = $request->withAddedHeader('X-Php-Follow-Location', '0');
		}
		catch(InvalidArgumentException $exc)
		{
			// nothing to do
		}
		
		try
		{
			$response = $this->_client->sendRequest($request);
		}
		catch(ClientExceptionInterface $exc)
		{
			return new ArrayIterator();
		}
		
		$uris = $this->getUrisFromLocationHeader($response);
		if(!empty($uris))
		{
			return new ArrayIterator($uris);
		}
		
		$uris = $this->getUrisFromFrameContent($response);
		
		return new ArrayIterator($uris);
	}
	
	/**
	 * Normalizes the given url by setting the standard hostname and path.
	 * 
	 * @param UriInterface $sourceUrl
	 * @return UriInterface
	 */
	public function normalizeUrl(UriInterface $sourceUrl) : UriInterface
	{
		// normalization
		$matches = [];
		if(\preg_match('#^(.+)\\.atu\\.ca$#', $sourceUrl->getHost(), $matches))
		{
			try
			{
				$sourceUrl = $sourceUrl->withHost('atu.ca');
			}
			catch(InvalidArgumentException $exc)
			{
				// nothing to do
			}
			
			/** @phpstan-ignore-next-line */
			if(isset($matches[1]))
			{
				try
				{
					$sourceUrl = $sourceUrl->withPath($matches[1]);
				}
				catch(InvalidArgumentException $exc)
				{
					// nothing to do
				}
			}
		}
		
		try
		{
			// easyurl does not supports https
			$sourceUrl = $sourceUrl->withScheme('http');
		}
		catch(InvalidArgumentException $exc)
		{
			// nothing to do
		}
		
		return $sourceUrl;
	}
	
	/**
	 * Gets the uris from the location header.
	 * 
	 * @param ResponseInterface $response
	 * @return array<integer, UriInterface>
	 */
	public function getUrisFromLocationHeader(ResponseInterface $response) : array
	{
		$uris = [];
		$locationHeader = $response->getHeader('Location');
		if(0 < \count($locationHeader))
		{
			foreach($locationHeader as $locationPart)
			{
				try
				{
					$uris[] = $this->_uriFactory->createUri($locationPart);
				}
				catch(InvalidArgumentException $exc)
				{
					// nothing to do
				}
			}
		}
		
		return $uris;
	}
	
	/**
	 * Gets the uris from the http contents.
	 *
	 * @param ResponseInterface $response
	 * @return array<integer, UriInterface>
	 */
	public function getUrisFromFrameContent(ResponseInterface $response) : array
	{
		$uris = [];
		// if not found in the headers, may be it's in the html body
		// as easyurl permits to handle the page in a frame
		// find the tag  <frame name="main" src="___the_url_here___">
		$res = $response->getBody()->__toString();
		$pos = \mb_strpos($res, '<frame name="main" src="');
		if(false !== $pos)
		{
			$rpos = \mb_strpos($res, '"', $pos + 28);
			if(false !== $rpos)
			{
				$url = \trim((string) \mb_substr($res, $pos + 24, $rpos - $pos - 24));
				
				try
				{
					$uris[] = $this->_uriFactory->createUri($url);
				}
				catch(InvalidArgumentException $exc)
				{
					// nothing to do
				}
			}
		}
		
		return $uris;
	}
	
}
