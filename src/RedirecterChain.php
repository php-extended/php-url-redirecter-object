<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-url-redirecter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UrlRedirecter;

use ArrayIterator;
use Iterator;
use Psr\Http\Message\UriInterface;
use RuntimeException;

/**
 * RedirecterChain class file.
 * 
 * This class represents the aggregation of multiple redirecters.
 * 
 * @author Anastaszor
 */
class RedirecterChain implements RedirecterInterface
{
	
	/**
	 * The inner redirecters.
	 * 
	 * @var array<integer, RedirecterInterface>
	 */
	protected array $_inner = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a redirecter to the chain.
	 * 
	 * @param RedirecterInterface $redirecter
	 * @return RedirecterChain
	 */
	public function addRedirecter(RedirecterInterface $redirecter) : RedirecterChain
	{
		$this->_inner[] = $redirecter;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UrlRedirecter\RedirecterInterface::accept()
	 */
	public function accept(?UriInterface $sourceUrl) : bool
	{
		foreach($this->_inner as $redirecter)
		{
			if($redirecter->accept($sourceUrl))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UrlRedirecter\RedirecterInterface::getRedirections()
	 */
	public function getRedirections(?UriInterface $sourceUrl) : Iterator
	{
		$uris = [];
		
		foreach($this->_inner as $redirecter)
		{
			try
			{
				$newUris = $redirecter->getRedirections($sourceUrl);
				
				foreach($newUris as $uri)
				{
					$uris[] = $uri;
				}
			}
			catch(RuntimeException $exc)
			{
				// gets next
			}
		}
		
		return new ArrayIterator($uris);
	}
	
}
