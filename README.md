# php-extended/php-url-redirecter-object

Library to retrieve hidden links from any simple redirecter website

![coverage](https://gitlab.com/php-extended/php-url-redirecter-object/badges/main/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-url-redirecter-object/badges/main/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-url-redirecter-object ^8`


## Supported domains


### https requests

- app.link (branch.io)
- atu.ca
- bit.do
- bit.ly
- buff.ly
- clk.im
- geni.us
- goo.gl
- is.gd
- ity.im
- lmgtfy.com
- ovh.to
- po.st
- psbe.co
- qr.net
- rebrand.ly	(rebrandly.click, rebrandly.xyz)
- securitylinks.com
- short.cm
- smarturl.it
- snip.ly
- snipli.com
- t.co
- tiny.cc
- tinyurl.com
- tr.im
- ushortlinks.com
- urlsgalore.com
- v.gd
- youtu.be


### http requests

- 9nl.at (clickmeter.com)
- beam.to
- bfy.tw
- clkme.in
- cutt.us
- cur.lv
- easyurl.net
- georiot.co
- goshrink.com
- hyperurl.co
- mcaf.ee
- ow.ly
- redirects.ca
- shorl.com
- slink.co
- snip.li
- tiny.pl
- u.to
- utm.io


## Basic Usage

You may use this library the following way:

```php

use PhpExtended\UrlRedirecter\RedirecterFactory;

$factory = new RedirecterFactory();
$redirecter = $factory->createRedirecter();
// $redirecter instanceof RedirecterInterface

```


## License

MIT (See [license file](LICENSE)).
