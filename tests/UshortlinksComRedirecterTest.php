<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-url-redirecter-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\UrlRedirecter\UshortlinksComRedirecter;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;

/**
 * UshortlinksComRedirecterTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\UrlRedirecter\UshortlinksComRedirecter
 *
 * @internal
 *
 * @small
 */
class UshortlinksComRedirecterTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UshortlinksComRedirecter
	 */
	protected UshortlinksComRedirecter $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UshortlinksComRedirecter(
			$this->getMockForAbstractClass(ClientInterface::class),
			$this->getMockForAbstractClass(RequestFactoryInterface::class),
			$this->getMockForAbstractClass(UriFactoryInterface::class),
		);
	}
	
}
